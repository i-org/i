尔雅国际化编程语言促进会 PL18N.org
==================================

![pl18n-logo](pl18n.png)

尔雅国际化编程语言促进会：一个致力于推动编程语言国际化的组织。

PL18N.org = International Programming Language Stardard Promotion Org

![pl18n-logo](pl18n-standard-n.png)



### 标志说明： ###

标志颜色定义，参考奥林匹克五环色，南极洲也是编程语言需要使用的地方，所以比奥运多了一色：
+ 天蓝色代表欧洲，黄色代表亚洲，红色代表美洲
+ 赤道：代表沟通
+ 黑色代表非洲，草绿色代表澳洲，南极洲银灰





