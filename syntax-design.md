友码系统设定
========
状态含义：●完成 ■待测试 ◆被认领开发中 ★计划

文件头部设定
------------

    ※ -*- coding: utf-8 il:zh-jobinson -*-

必须：在源码文件开头声明设定所使用语言版本

各语言标识对应：

    中文：
    zh-jobinson  语言设计者黑传说常用的中文编程词汇
    zh-dao 　　  道教用语版
    zh-fo 　　   佛教用语版
    zh-wenyan    文言文版
    
    英文：
    en-jobinson 　　语言设计者黑传说常用的英文编程词汇
    en-commonlisp 　通链版词汇


# 功能系统 #

+ 首.尾系统
+ 类型设定系统：可选
+ 宏
+ 统筹法系统（有必要么？）

+ 实时解析器
+ 内建编译器


## 语法设定

TODO: 语法设定仍然需要完善

### 基本形式设定

+ 前置标识表达式：全部使用全角符号

        ｛操作 内容｝
        
        ｛[命令名]+[操作选定]+[操作目标]+[结果筛选参数]｝
        ｛[function]+[operation]+[target]+[option]｝
                
        ｛设函数 ｛甲 乙 &可选参数 &关键字参数｝｛内容｝｝


+ 注释方式：——最好不要用对称符号，不方便。

        \*  *\
        ※

+ 字符设定：

        英文字母：a b c d e f g h i j k l m n o p q r s t u v w s y z
        对应
        中文笔画：横竖撇奈 竖直弯勾
        ㇐ ㇑ ㇓ ㇏㇔ ㇕ ㇖ ㇗ ㇘ ㇙ ㇚ ㇛ ㇜

+ 命名机制：
实际上后台是以数字形式命名，前台使用的是符号语义化的语言。以数字名字作为友码通用兼容层，比如：

        say == 001 == 说

+ 命名约定

-------------------------------------------------------------------------------

下面可参考神编和scheme，尽可能精炼

## 数据类型：

+ 判断：真 无 假  booleans T nil F
+ 字符：  strings  字串
+ 数字:  numbers
+ 数列:  lists   ｛｝
+ :tuples
+ :closures
+ :continuations

+ 文件 streams

+ 函数： functions

+ 名讳 symbols

+ 错误信息： errors 提示 prompts



## 常用函数：
### 条件结构：
+ 若 if 
+ 与 and 
+ 或 or 
+ 当 cond

### 流程控制
+ 标识
+ 必然运行
+ 然后 接下去干什么——包括跳转到、跳出


### 数据操作：
+ 数据类型判断
+ 数据转换
+ 数据遍历
+ 数据提取

### 赋值
+ 设 set
+ 获取值 get-value


### 前置标识表达式操作
+ 构建 cons
+ 取首 first
+ 取余 rest
+ 判断是否为数列 consp ?cons

### 函数构建
+ 定义函数 defun
+ 省名函数 lambda
+ 局部赋值 let
+ 比较 =
+ 类型设定 type


### 提醒处理  error prompt 一般用于提示出错信息
+ 提示 prompt  （error放入此处）

### 矢量 vector
+ 创建
+ 数据位置
+ 矢量否？ vector?

### 文件操作
+ 逐字读取 read-byte
+ 输出为文件
+ 打开文件 open
+ 关闭文件 close

### 时间操作
+ 获得时间 get-time

### 数学操作
+ + 加 add
+ - 减 subtract
+ × 乘 * multiply
+ /  除 divide
+ >  大于 greater?
+ <  小于 less?
+ >=  大于等于 greater-than-or-equal-to?
+ <=  小于等于 less-than-or-equal-to?
+ 判断是否是数字 number?

### 二进制操作
+ 进制转换 
+ 取位
+ 换位
+ 计算（加 减 乘 除等）
+ 比较


## 统筹法系统

+ 抽象方法 defgeneric
+ 方法 defmethod
+ 抽象类 defclass





